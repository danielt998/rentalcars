import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import java.io.FileReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class Main{
  public static List<Vehicle> vehicleList = new ArrayList<Vehicle>();
  public static void main(String[] args){
    String fileName;
    if(args.length==0){
      fileName="vehicles.json";
    }else{
      fileName=args[0];
    }
    parseVehicleXML(fileName);

    //first sort by price
    Collections.sort(vehicleList);
    for(Vehicle vehicle:vehicleList){
      System.out.println(vehicle.getName() + "-" + vehicle.getPrice());
    }

    //then print out vehicles with parsed SIPP
    for (Vehicle vehicle:vehicleList){
      System.out.println(vehicle.getName() + '-' + vehicle.getSipp() + '-' + vehicle.getCarType() + '-' + vehicle.getDoors() + '-' +
                         vehicle.getTransmission() + '-' + vehicle.getFuel() + '-' + (vehicle.getAirCon()?"AC":"no AC"));
  

    }
    //then print each supplier's cars, sorted by rating
    printSuppliers();
    printScores();
  }

  public static class Score{
    String name;
    int score;
    public Score(String s, int n){
      name = s;
      score = n;
    }
  }
  public static List<Score> vehicleScores=new ArrayList<Score>();

  public static Score getScore(String vehicleName){
    boolean contains=false;
    for(Score score:vehicleScores){
      if (score.name.equals(vehicleName)){
        contains=true;
        return score;
      }
    }
    if(!contains){
      Score s = new Score(vehicleName,0);
      vehicleScores.add(s);
      return s;
    }
    return null;//should never happen
  }

  public static void printScores(){
    for(Vehicle vehicle:vehicleList){
      int score=0;
      if(vehicle.getTransmission().equals("Manual")){
        score += 5;
      }else{
        score += 1;
      }

      if(vehicle.getAirCon()){
        score += 2;
      }
      vehicle.setScore(score);
      getScore(vehicle.getName()).score += score;
    }
    //now we have calculated the total scores, print out the results
    for(Vehicle vehicle:vehicleList){
      System.out.println(vehicle.getName() + '-' + vehicle.getScore() + '-' + vehicle.getRating() + '-' +
                                                                              getScore(vehicle.getName()).score);
    }
  }

  private static final String[] carTypes=
                                {"Mini","Economy","Compact","Intermediate","Standard","Full Size","Premium","Luxury","Special"};
  public static void printSuppliers(){
    for (String currentType:carTypes){
      //for now, O(carTypes.length * n) is fine, though would implement bucketSort or similar as a longer term solution
      List<Vehicle> list=new ArrayList<Vehicle>();
      for (Vehicle vehicle:vehicleList){
        if (vehicle.getCarType().equals(currentType)){
          list.add(vehicle);
        }
      }
System.out.println("list.size():"+list.size());
      list.sort(new Comparator<Vehicle>(){
        @Override
        public int compare(Vehicle vehicle1,Vehicle vehicle2){
          return vehicle1.getRating() > vehicle2.getRating() ? 1 : -1;
        }
      });
//      for(Vehicle vehicle:vehicleList){
      if(list.size()==0){
        continue;
      }
      System.out.println(list.get(list.size()-1).getName() + '-' + list.get(list.size()-1).getCarType() + '-' +
                         list.get(list.size()-1).getSupplier() + '-' + list.get(list.size()-1).getRating());
//      }
    }
  }
  public static void parseVehicleXML(String fileName){
    try{
      JSONParser parser = new JSONParser();
      JSONObject jsonObject=(JSONObject)parser.parse(new FileReader(fileName));
      JSONArray vehicleArray = (JSONArray)(((JSONObject)jsonObject.get("Search")).get("VehicleList"));
      Iterator iterator = vehicleArray.iterator();

      while(iterator.hasNext()){
        JSONObject vecObj = (JSONObject)iterator.next();
        Vehicle vehicle = new Vehicle((String)vecObj.get("sipp"),
                                      (String)vecObj.get("name"),
                                      ((Number)(vecObj.get("price"))).doubleValue(),//Number is super class of Long,Double
                                      (String)vecObj.get("supplier"),
                                      ((Number)(vecObj.get("rating"))).doubleValue()
                                      );
        vehicleList.add(vehicle);
      }
 
    }catch(Exception e){
      e.printStackTrace();
    }
  }
 
}
