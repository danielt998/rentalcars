public class Vehicle implements Comparable<Vehicle>{
  private final String sipp;
  private final String name;
  private final double price;
  private final String supplier;
  private final double rating;

  private String carType;
  private String doors;
  private String transmission;
  private String fuel;
  private boolean airCon;
  private int score;
  public Vehicle(String givenSipp,String givenName,double givenPrice,String givenSupplier,double givenRating){
    sipp=givenSipp;
    name=givenName;
    price=givenPrice;
    supplier=givenSupplier;
    rating=givenRating;
    parseCDMR();
  }

  //to compare by price only...
  @Override
  public int compareTo(Vehicle otherVehicle){
    if(this.price < otherVehicle.price){
      return -1;
    }else{
      return 1;
    }
  }
  private void parseCDMR(){
    switch(sipp.charAt(0)){
      case 'M':carType="Mini";break;
      case 'E':carType="Economy";break;
      case 'C':carType="Compact";break;
      case 'I':carType="Intermediate";break;
      case 'S':carType="Standard";break;
      case 'F':carType="Full Size";break;
      case 'P':carType="Premium";break;
      case 'L':carType="Luxury";break;
      case 'X':carType="Special";break;
    }

    switch(sipp.charAt(1)){
      case 'B':doors="2 doors";break;
      case 'C':doors="4 doors";break;
      case 'D':doors="5 doors";break;
      case 'W':doors="Estate";break;
      case 'T':doors="Convertible";break;
      case 'F':doors="SUV";break;
      case 'P':doors="Pick Up";break;
      case 'V':doors="Passenger Van";break;
    }
    switch(sipp.charAt(2)){
      case 'M':transmission="Manual";break;
      case 'A':transmission="Automatic";break;
    }
    switch(sipp.charAt(3)){
      case 'N':fuel="Petrol";
               airCon=false;
               break;
      case 'R':fuel="Petrol";
               airCon=true;
               break;
    }
  }
  public void setScore(int givenScore){
    score = givenScore;
  }
  public int getScore(){
    return score;
  }
  public String getCarType(){
    return carType;
  }
  public String getDoors(){
    return doors;
  }
  public String getTransmission(){
    return transmission;
  }
  public String getFuel(){
    return fuel;
  }
  public boolean getAirCon(){
    return airCon;
  }
  public String getSipp(){
    return sipp;
  }
  public String getName(){
    return name;
  }
  public double getPrice(){
    return price;
  }
  public String getSupplier(){
    return supplier;
  }
  public double getRating(){
    return rating;
  }
}
