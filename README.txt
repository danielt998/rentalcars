Please download json-simple-1.1.1.jar from here:https://code.google.com/archive/p/json-simple/downloads and add it to your classpath

Then run javac *.java to compile and java Main to run. Optionally, you can specify the json file to use as the first parameter, otherwise it will default to vehicles.json
